package com.formation.service;

public class MessageBulder {

	
	
	public String getMessage(String name) {
		
		StringBuilder result= new StringBuilder();
		
		if (name == null || name.trim().length() == 0) {
			result.append("plz provide a name");
		}else {
			result.append("hi "+name );
		}
		
		return result.toString();
	}
}
